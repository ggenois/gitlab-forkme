try {
  var gitlabf = {
    init : function() {
      var src = document.getElementById("gitlab-forkme").getAttribute("forksrc");
      var elemLink = document.createElement('a');
      elemLink.style.cssText = 'text-decoration: none;';
      elemLink.setAttribute("id", "gitlab-forkme-link");
      elemLink.setAttribute("target", "_blank");
      elemLink.setAttribute("href", src);
      var elemDiv = document.createElement('span');
      elemDiv.style.cssText = 'font-family: sans-serif; width: 200px; ' +
        'text-align:center; letter-spacing: -1px; font-size: 1.2em; ' +
        'position:fixed; top:55px; right:-75px; display:block; ' +
        '-webkit-transform: rotate(45deg); -moz-transform: rotate(45deg); ' +
        'background-color:black; color:white; padding: 6px 50px; z-index:99';
      elemDiv.setAttribute("id", "gitlab-forkme-element");
      elemDiv.innerHTML = "Fork me on Gitlab"
      elemLink.appendChild(elemDiv);
      document.body.appendChild(elemLink);
    }
  }
  gitlabf.init();
} catch (e) {
  console.info("gitlab-forkme.js just crashed. Here's the error(s):");
  console.info(e);
}