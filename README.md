# Gitlab Fork-me

I found [this](https://gitlab.com/seanwasere/fork-me-on-gitlab) and wanted
to do a "JS-Injected" version to "simplify" it for my own projects.

Exemple of implementation:

```
<script src="gitlab-forkme.js" id="gitlab-forkme" forksrc="https://git.geno.is/root/"></script>
```

The objective: Centralise the gitlab-forkme.js via a CDN and then upgrade
everything at once when it's needed.